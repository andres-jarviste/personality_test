module.exports = class Question {
	constructor({id, question}) {
		this._id = id;
		this._question = question;
		this._variants = [];
	}

	get id() {
		return this._id;
	}

	get question() {
		return this._question;
	}

	get variants() {
		return this._variants;
	}

	addAnswer(answerObj) {
		this._variants.push(answerObj);
		return this;
	}

	toJSON() {
		return {
			id: this._id,
			question: this._question,
			variants: this._variants,
		};
	}
};
