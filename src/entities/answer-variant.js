module.exports = class AnswerVariant {
	constructor({id, description, weight}) {
		this._id = id;
		this._description = description;
		this._weight = weight;
	}

	get id() {
		return this._id;
	}
	get description() {
		return this._description;
	}
	get weight() {
		return this._weight;
	}

	toJSON() {
		return {
			id: this._id,
			description: this._description,
			weight: this._weight,
		};
	}
};
