const questions = {
	"89bde201-735e-49bb-acea-0194c62f73b8": {
		question: "You’re really busy at work and a colleague is telling you their life story and personal woes. You:",
		answers: {
			"d9624d45-120c-468d-a72c-e46cd4818561": {
				description: "Don’t dare to interrupt them",
				weight: -2,
			},
			"3321a1db-586e-4790-9b28-a58f0717a40d": {
				description: "Think it’s more important to give them some of your time; work can wait",
				weight: -1,
			},
			"f3ffbfe4-8fe2-4bbb-8162-51c1593be284": {
				description: "Listen, but with only with half an ear",
				weight: 1,
			},
			"3cfdeaa7-0216-4640-8d1d-32694a4b2f39": {
				description: "Interrupt and explain that you are really busy at the moment",
				weight: 2,
			},
		},
	},
	"45b0b8ae-b7c3-417e-bacd-98f715215a24": {
		question: "You’ve been sitting in the doctor’s waiting room for more than 25 minutes. You:",
		answers: {
			"e2e766bb-44d4-4824-a2c5-d2c61ef98771": {
				description: "Look at your watch every two minutes",
				weight: -2,
			},
			"6ba9c5f6-1698-445e-8e28-45e4559b2417": {
				description: "Bubble with inner anger, but keep quiet",
				weight: -1,
			},
			"999b302a-e0f7-499c-b5a7-3267b44c87e5": {
				description: "Explain to other equally impatient people in the room that the doctor is always running late",
				weight: 1,
			},
			"b0f604bd-55f7-414a-b6d1-ba05ddc871b1": {
				description: "Complain in a loud voice, while tapping your foot impatiently",
				weight: 2,
			},
		},
	},
	"c0d3dc35-2194-4aae-aebd-ed68cbc9b016": {
		question: "You’re having an animated discussion with a colleague regarding a project that you’re in charge of. You:",
		answers: {
			"0611e6ce-0e79-4876-b6af-ecb20123dbd4": {
				description: "Don’t dare contradict them",
				weight: -2,
			},
			"18d41e53-6d07-4456-a2f3-f3300eb525d4": {
				description: "Think that they are obviously right",
				weight: -1,
			},
			"cd9cc7e2-c9f5-46ea-b5af-49534c547f75": {
				description: "Defend your own point of view, tooth and nail",
				weight: 1,
			},
			"eacfd5fa-796e-4d5c-818c-9b4d67e729e6": {
				description: "Continuously interrupt your colleague",
				weight: 2,
			},
		},
	},
	"4ba00416-a9bf-4174-bc08-7569b86027c1": {
		question: "You are taking part in a guided tour of a museum. You:",
		answers: {
			"665fe05a-efcf-49ef-9fb2-cd7ec63795d1": {
				description: "Are a bit too far towards the back so don’t really hear what the guide is saying",
				weight: -2,
			},
			"36b32f87-12dc-48d9-a1e9-e4087579efe7": {
				description: "Follow the group without question",
				weight: -1,
			},
			"0e3b586c-181c-4625-8092-392a4bc462dd": {
				description: "Make sure that everyone is able to hear properly",
				weight: 1,
			},
			"3f8cba02-59a3-4452-b2c5-f3ae9d099c29": {
				description: "Are right up the front, adding your own comments in a loud voice",
				weight: 2,
			},
		},
	},
	"66efcdf8-59a6-458f-b149-b910d055947c": {
		question: "During dinner parties at your home, you have a hard time with people who:",
		answers: {
			"a8eafb84-65fb-44e9-8d16-e79d29741e5c": {
				description: "Ask you to tell a story in front of everyone else",
				weight: -2,
			},
			"32659ec1-962b-4557-b39c-3aeaa32a8a08": {
				description: "Talk privately between themselves",
				weight: -1,
			},
			"0965f1f8-e92a-4fda-aa41-16cb0788a8af": {
				description: "Hang around you all evening",
				weight: 1,
			},
			"6a3e1661-a4e3-49e1-b87d-6958ddd443e3": {
				description: "Always drag the conversation back to themselves",
				weight: 2,
			},
		},
	},
	"b2cb9347-2af6-4911-970b-ee0da3a6ee5f": {
		question: "You crack a joke at work, but nobody seems to have noticed. You:",
		answers: {
			"61c207ec-41ce-4a22-8e73-ff60fb676e48": {
				description: "Think it’s for the best — it was a lame joke anyway",
				weight: -2,
			},
			"9ce506a4-7049-4277-8bd5-638e9fca5990": {
				description: "Wait to share it with your friends after work",
				weight: -1,
			},
			"52e643c3-df64-45f5-a06e-8e6491fd1e64": {
				description: "Try again a bit later with one of your colleagues",
				weight: 1,
			},
			"cfc391e4-0971-4c87-986c-8ac1050d4b2d": {
				description: "Keep telling it until they pay attention",
				weight: 2,
			},
		},
	},
	"fc4ef33a-c0f7-4ebd-a6f8-e49b6a408668": {
		question: "This morning, your agenda seems to be free. You:",
		answers: {
			"baf42686-fc76-49ec-87ce-481a385f2e01": {
				description: "Know that somebody will find a reason to come and bother you",
				weight: -2,
			},
			"0eedd020-89d2-4125-881d-b20ceb9cd13b": {
				description: "Heave a sigh of relief and look forward to a day without stress",
				weight: -1,
			},
			"8e53f142-d772-4ac4-a931-3bc6e8f85d2b": {
				description: "Question your colleagues about a project that’s been worrying you",
				weight: 1,
			},
			"1905f5b2-785a-4a70-bd96-2ebdb0213a5f": {
				description: "",
				weight: 2,
			},
		},
	},
};

module.exports = {
	questions,
};

/*
"": {
    question: "",
    answers: {
        "": {
            description: "",
            weight: -2
        },
        "": {
            description: "",
            weight: -1
        },
        "": {
            description: "",
            weight: 1
        },
        "": {
            description: "",
            weight: 2
        },
    }
},

*/
