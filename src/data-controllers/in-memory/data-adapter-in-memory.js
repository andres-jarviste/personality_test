const _questions = require("./data.js").questions;
const Question = require("../../entities/question");
const AnswerVariant = require("../../entities/answer-variant");

function getQuestionsIds() {
	return Object.keys(_questions);
}

function getQuestion(uuid) {
	const questionData = _questions[uuid];
	if (!questionData) {
		return false;
	}
	const question = new Question({
		id: uuid,
		question: questionData.question,
	});
	Object.keys(questionData.answers).forEach((variantUuid) => {
		const variant = questionData.answers[variantUuid];
		question.addAnswer(
			new AnswerVariant({
				id: variantUuid,
				description: variant.description,
				weight: variant.weight,
			}),
		);
	});

	return question;
}

function getAnswerWeight(questionId, answerId) {
	const questionData = _questions[questionId];
	if (!questionData) {
		throw new Error("Could not find question with id " + questionId);
	}

	const answerData = questionData.answers[answerId];
	if (!answerData) {
		throw new Error(`
			Could not find answer ${answerId} for question ${questionId}
		`);
	}

	return answerData.weight;
}

module.exports = {
	getQuestionsIds,
	getQuestion,
	getAnswerWeight,
};
