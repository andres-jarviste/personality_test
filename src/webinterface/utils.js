const ResultOutput = require("../usecases/models/result-output");
const ErrorOutput = require("../usecases/models/error-output");
const inMemoryDataAdapter = require("../data-controllers/in-memory/data-adapter-in-memory");

const errorCodesMap = {
	"ERROR_VALIDATION": 406,
	"ERROR_UNEXPECTED": 400,
};

function _outputError(code, errorMessage, res) {
	res.status(code).json({
		errors: [
			{message: errorMessage},
		],
	});
}

async function callUseCase(useCaseFunction, params, res) {
	try {
		result = await useCaseFunction(params, inMemoryDataAdapter);
		if (result instanceof ErrorOutput) {
			_outputError(
				errorCodesMap[result.errorType] || 400,
				result.errorMessage,
				res,
			);
			return;
		}
		if (result instanceof ResultOutput) {
			res.status(200).json(result.result);
			return;
		}
		_outputError(400, "Unexpected result from function " + useCaseFunction.name, res);
	} catch (error) {
		_outputError(500, error.message, res);
	}
}

module.exports = {
	callUseCase,
};
