module.exports = function() {
	const publishRandomQuestions = require("../../../../src/usecases/publish-random-questions");
	const {callUseCase} = require("../../utils");
	const operations = {
		GET,
	};

	function GET(req, res) {
		callUseCase(
			publishRandomQuestions,
			{numberOfQuestions: req.query.count},
			res,
		);
	}

	GET.apiDoc = {
		summary: "Fetch questions",
		operationId: "getQuestions",
		description: "Fetch random 4 questions as an array",
		parameters: [
			{
				in: "query",
				description: "Number of required questions",
				type: "integer",
				name: "count",
				required: true,
			},
		],
		responses: {
			200: {
				description: "List of question objects",
				schema: {
					type: "array",
					items: {
						type: "object",
						properties: {
							id: {type: "string"},
							question: {type: "string"},
							answers: {
								type: "array",
								items: {
									$ref: "#/definitions/Answer",
								},
							},
						},
					},
				},
			},
		},
	};

	return operations;
};
