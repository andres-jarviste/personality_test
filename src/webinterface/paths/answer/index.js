const evaluateAnswers = require("../../../../src/usecases/evaluate_aswers");
const {callUseCase} = require("../../utils");

module.exports = function() {
	const operations = {
		POST,
	};

	function POST(req, res) {
		callUseCase(
			evaluateAnswers,
			{answers: req.body},
			res,
		);
		console.log(`About to submit answer: ${JSON.stringify(req.body)}`);
	}

	POST.apiDoc = {
		summary: "Submit answers to questions",
		operationId: "submitAnswer",
		consumes: ["application/json"],
		parameters: [
			{
				in: "body",
				name: "answer",
				schema: {
					type: "array",
					items: {
						type: "object",
						properties: {
							questionId: {type: "string"},
							answerId: {type: "string"},
						},
					},
				},
			},
		],
		responses: {
			200: {
				description: "Evaluation of the answers",
				schema: {
					type: "object",
					properties: {
						evaluation: {type: "string"},
					},
				},
			},
		},
	};

	return operations;
};
