const apiDocs = {
	swagger: "2.0",
	basePath: "/api",
	info: {
		title: "Personlity Tester API",
		version: "1.0.0",
	},
	definitions: {
		Answer: {
			type: "object",
			properties: {
				id: {type: "string"},
				description: {type: "string"},
				weight: {type: "integer"},
			},
		},
	},
	paths: {},
};

module.exports = apiDocs;
