const ErrorOutput = require("./models/error-output");
const ResultOutput = require("./models/result-output");

function evaluateAnswers({answers}, dataAdapter) {
	try {
		if (!Array.isArray(answers)) {
			return new ErrorOutput({
				errorType: ErrorOutput.ERRORTYPE_VALIDATION,
				errorMessage: "Parameter answer not valid for evaluateAnswers",
			});
		}
		const weightsTotal = answers.reduce((cumulativeValue, current) => {
			return cumulativeValue + dataAdapter.getAnswerWeight(
				current.questionId, current.answerId,
			);
		},
		0);

		let resultMessage = "We could not decide who you are";
		if (weightsTotal < 0) {
			resultMessage = "Slightly introvert";
			if (weightsTotal < -4) {
				resultMessage = "Definitely introvert";
			}
		} else if (weightsTotal > 0) {
			resultMessage = "Slightly extrovert";
			if (weightsTotal > 4) {
				resultMessage = "Definitely extrovert";
			}
		}
		return new ResultOutput({
			result: {evaluation: resultMessage},
		});
	} catch (error) {
		return new ErrorOutput({
			errorType: ErrorOutput.ERRORTYPE_UNEXPECTED,
			errorMessage: error.message,
		});
	}
}
module.exports = evaluateAnswers;
