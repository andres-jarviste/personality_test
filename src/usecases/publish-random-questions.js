const ErrorOutput = require("./models/error-output");
const ResultOutput = require("./models/result-output");

function publishRandomQuestions({numberOfQuestions}, dataAdapter) {
	try {
		const questionIds = dataAdapter.getQuestionsIds();
		if (numberOfQuestions > questionIds.length) {
			return new ErrorOutput({
				errorType: ErrorOutput.ERRORTYPE_VALIDATION,
				errorMessage: "Can not get this number of quesitons",
			});
		}

		const selectedIds = new Set();
		const result = [];
		while (selectedIds.size < numberOfQuestions) {
			const randomIndex = Math.floor(Math.random() * numberOfQuestions);
			const selectedId = questionIds[randomIndex];
			if (!selectedIds.has(selectedId)) {
				selectedIds.add(selectedId);
				result.push(dataAdapter.getQuestion(selectedId));
			}
		}
		return new ResultOutput({result});
	} catch (error) {
		return new ErrorOutput({
			errorType: ErrorOutput.ERRORTYPE_UNEXPECTED,
			errorMessage: "Unexpected error: " + error.message,
		});
	}
}

module.exports = publishRandomQuestions;
