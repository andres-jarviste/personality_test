module.exports = class ErrorOutput {
	static ERRORTYPE_VALIDATION = "ERROR_VALIDATION";
	static ERRORTYPE_UNEXPECTED = "ERROR_UNEXPECTED";
	constructor({errorType, errorMessage}) {
		this.errorType = errorType;
		this.errorMessage = errorMessage;
	}
	setDetails(details) {
		this.details = details;
		return false;
	}
};
