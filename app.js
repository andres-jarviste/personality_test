const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const {initialize} = require("express-openapi");
const swaggerUi = require("swagger-ui-express");

const app = express();

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.resolve(__dirname, "client/build")));

// OpenAPI routes
initialize({
	app,
	apiDoc: require("./src/webinterface/api-docs"),
	paths: "./src/webinterface/paths",
	errorMiddleware: function(err, req, res, next) {
		res.status(err.status).json({errors: err.errors});
		return;
	},
});

// OpenAPI UI
app.use(
	"/api-documentation",
	swaggerUi.serve,
	swaggerUi.setup(null, {
		swaggerOptions: {
			url: "/api/api-docs",
		},
	}),
);

module.exports = app;
