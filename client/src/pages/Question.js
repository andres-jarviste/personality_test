import {useParams, useNavigate, Navigate} from "react-router-dom";

const Question = ({
    questions, answers, loaded, loading, submitAnswers, answerQuestion, errorMsg
}) => {
    let { questionNo } = useParams();
    const navigate = useNavigate();
    if (errorMsg) {
        return <Navigate to="/error" />;
    }
    if (!loading && !loaded) {
        return <Navigate to="/" />;
    }
    if (loading) {
        return (
            <div>Loading ...</div>
        )
    }

    const currentIndex = parseInt(questionNo)-1;
    const question = questions[currentIndex];
    const nextQuestion = () => {
        navigate("/question/" + (currentIndex+2), {replace: false})
    }

    const previousQuestion = () => {
        navigate("/question/" + (currentIndex), {replace: false})
    }
    
    const markers = "ABCDEFGHIJKLMNOPQR"
    const currentAnswer = answers[question.id] || null;
    const answersJsx = question.variants.map((variant, index) => {
        let itemStyleClass = "info-item"
        if (currentAnswer === variant.id) {
            itemStyleClass += ' selected-answer'
        }
        return (
            <div 
                className={itemStyleClass} 
                key={variant.id}
                onClick={()=> answerQuestion(question.id, variant.id)}
            >
                <h4>{markers[index]}</h4>
                <p>{variant.description}</p>
            </div>            
        )
    })

    let firstButton = <button onClick={previousQuestion} className="main-action-button">Previous</button>
    if (currentIndex === 0) {
        firstButton = (
            <button 
                onClick={()=>navigate("/",{replace: false})} 
                className="main-action-button"
            >
                Restart
            </button>
        )
    }
    
    let secondButton = (
        <button 
            onClick={nextQuestion} 
            className="main-action-button"
            disabled={!currentAnswer}
        >
                Next
        </button>
    )
    if (questions.length === (currentIndex+1)) {
        secondButton = (
            <button 
                onClick={()=> {
                    submitAnswers();
                    navigate("/conclusion", {replace: true})
                }} 
                className="main-action-button"
                disabled={!currentAnswer}
            >
                Finish
            </button>
        )
    }

    return (
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-10 col-md-9 col-lg-8 align-self-center">
                        <div className="section-heading">
                            <h6>Test project by Andres Järviste</h6>
                            <h4>Question <em>{questionNo}/{questions.length}</em></h4>
                            <div className="question-description">
                                {question.question}
                            </div>
                        </div>
                        <section className="row get-info">
                            {answersJsx}
                        </section>
                    </div>   
                    <div className="col-12 question-action">
                        {firstButton}
                        {secondButton}
                    </div>             
                </div>
            </div>      
    )
}

export default Question;