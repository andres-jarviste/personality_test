
import {useNavigate} from "react-router-dom";

const Intro = ({readQuestions, loaded}) => {
    const navigate = useNavigate();
    const handleButton = () => {
        readQuestions();
        navigate("/question/1", {replace: false})
    }
    
    return (
        <div className="main-banner" id="top">
        <div className="container">
          <div className="row">
            <div className="col-lg-6 align-self-center">
              <div className="header-text">
                <h6>Test project by Andres Järviste</h6>
                <h2>Are you an introvert or extrovert?<br />
                <em>Lets find out!</em></h2>
                <div className="main-button-gradient">
                  <div className="scroll-to-section">
                    <button className="main-action-button" onClick={handleButton}>
                        Start the Tester
                    </button>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-6">
              <div className="right-image">
                <img src="/assets/images/banner-right-image.png" alt=""/>
              </div>
            </div>
          </div>
        </div>
        <div className="row">

        </div>
      </div>
    )
}
export default Intro