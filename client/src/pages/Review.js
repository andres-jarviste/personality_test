import {useNavigate, Navigate} from "react-router-dom";
function Review({conclusion, loading, errorMsg}) {
    const navigate = useNavigate();

    if (errorMsg) {
        return <Navigate to="/error" />;
    }
    if (!conclusion && !loading) {
        console.log("No conclusion, no loading in Review")
        return <Navigate to="/" />;
    }
    if (loading) {
        return (
            <div>Loading ...</div>
        )
    }
    
    return (
        <div className="container">
            <div className="row">
                <div className="col-lg-6">
                    <div className="left-image">
                        <img src="assets/images/about-left-image.png" alt=""/>
                    </div>
                </div>
                <div className="col-lg-6 align-self-center">
                    <div className="section-heading review">
                        <h6>Test project by Andres Järviste</h6>
                        <h4>And the result is</h4>
                        <h4><em>{conclusion}</em></h4>
                        <div>
                            <p>Want to try again?</p>
                            <button 
                                onClick={()=>navigate("/",{replace: true})} 
                                className="main-action-button"
                            >
                                Restart the test
                            </button>
                        </div>
                    </div>
                </div>                
            </div>
        </div>
    )
}
export default Review