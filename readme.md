# Test project "Personality tester" by Andres Järviste

Project goal was to build a simple personality test application, that takes 3-5 different questions, maps them into a score and produces a personality trait of either Introvert or Extrovert.

Project is deployed in free Heroku cloud [here](https://gentle-chamber-59384.herokuapp.com/)

Content for the project is copied from [psychologies.co.uk](https://www.psychologies.co.uk/self/are-you-an-introvert-or-an-extrovert.html)

Source code is in public GitLab repository [here](https://gitlab.com/andres-jarviste/personality_test)

Backend api description is available here: [https://gentle-chamber-59384.herokuapp.com/api-documentation/](https://gentle-chamber-59384.herokuapp.com/api-documentation/)

## About implementation
Project's architecture is somewhat overkill for such a small project and is more like architecture prototype that could be applied also to larger projects.

## Backend
Backend is implemented in Javascript with Express. In general principles of Clean Architecture are followed. There are following layers in the application
### Entities
Pure Javascript classes that represent data objects of the application
### Use Cases
Pure Javascript components that encapsulate business logic. Only dependecies for there components can be Entities. Use Cases are easily testable
### Data Adapter(s)
Data Adapter component is responsible to provide data for use cases according to the interface required by use cases. Data of the questions and answer variants is stored in memory at the moment, but can be easily substituted with other data storage thanks to this approach. Data adapters depend on Use Case definitions
### Web Interface
Component that listens for incoming requests. Validates the request parameters and calls Use Cases. Web interface API description and validation is implemented with the help of OpenAPI desscription of endpoints.

### Testing
Back end business logic is covered with unit tests. Also there are some web api interface tests to demonstrate how actual working api endpoints can be tested (integration tests)


## Frontend
Frontend is implemented in React and stored in the same repository ( folder /client).
Front end design is publicly availabe desing in [TemplateMo](https://templatemo.com/).
Front end fetches questions and sends answerts to back-end for evaluation. Result is returned from backend. Evaluation could be done also in front-end, but this approach gives a possibility to make analyses/reports based on the data.

### Local installation
Clone the repository. In main folder install dependecies and run the server (will run op port 8080):
```
npm install
npm start
````
When back-end is running, you can run the tests 
```
npm run tests
```
To install and run front end cd to front-end folder, install dependencies and run locally
```
cd client
npm install
npm start
```

Front end is now available on localhost at port 3000 and communicates with back end on port 8080

For deployment to live server build client repository and deploy all project (including /client/build) to the server. All static content of the front end is served from /client/build and should prepared with react build process (in ./client folder)
````
npm run build
````
