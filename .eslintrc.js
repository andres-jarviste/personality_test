module.exports = {
	env: {
		browser: true,
		commonjs: true,
		es2021: true,
	},
	extends: ["google", "plugin:react/recommended"],
	parserOptions: {
		ecmaVersion: "latest",
	},
	rules: {
		"max-len": ["error", {
			"code": 100,
			"ignoreStrings": true,
			"ignoreUrls": true,
		}],
		"indent": ["error", "tab"],
		"no-tabs": ["error", {allowIndentationTabs: true}],
		"quotes": ["error", "double"],
		"require-jsdoc": ["error", {
			"require": {
				"FunctionDeclaration": false,
				"MethodDefinition": false,
				"ClassDeclaration": false,
				"ArrowFunctionExpression": false,
				"FunctionExpression": false,
			},
		}],
	},
};
