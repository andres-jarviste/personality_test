const assert = require("assert");
const Question = require("../src/entities/question");
const AnswerVariant = require("../src/entities/answer-variant");

const isUuidRegexExp =
	/^[0-9a-fA-F]{8}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{12}$/gi;

const questions = require("../src/data-controllers/in-memory/data-adapter-in-memory");
describe("Data Adapter", function() {
	describe("#getQuestionsIds()", function() {
		it("should return array of question uuid's", function() {
			const received = questions.getQuestionsIds();
			assert.equal(Array.isArray(received), true);
			assert.equal(received.length > 0, true);
			const firstElement = received[0];
			assert.equal(typeof firstElement, "string");
			assert.equal(isUuidRegexExp.test(firstElement), true);
		});
	});
	describe("#getQuestion(questionUuid)", function() {
		it("should return object with data of one question. Question's uuid is parameter to the function", function() {
			const receivedResult = questions.getQuestion(
				"4ba00416-a9bf-4174-bc08-7569b86027c1",
			);
			const expectedResult = new Question({
				id: "4ba00416-a9bf-4174-bc08-7569b86027c1",
				question: "You are taking part in a guided tour of a museum. You:",
			});
			expectedResult.addAnswer(new AnswerVariant({
				id: "665fe05a-efcf-49ef-9fb2-cd7ec63795d1",
				description: "Are a bit too far towards the back so don’t really hear what the guide is saying",
				weight: -2,
			}));
			expectedResult.addAnswer(new AnswerVariant({
				id: "36b32f87-12dc-48d9-a1e9-e4087579efe7",
				description: "Follow the group without question",
				weight: -1,
			}));
			expectedResult.addAnswer(new AnswerVariant({
				id: "0e3b586c-181c-4625-8092-392a4bc462dd",
				description: "Make sure that everyone is able to hear properly",
				weight: 1,
			}));
			expectedResult.addAnswer(new AnswerVariant({
				id: "3f8cba02-59a3-4452-b2c5-f3ae9d099c29",
				description: "Are right up the front, adding your own comments in a loud voice",
				weight: 2,
			}));

			assert.deepEqual(expectedResult, receivedResult);
		});
	});
	describe("#getAnswerWeight(questionId, answerId", function() {
		it("should return correct weight", function() {
			const questionId = "89bde201-735e-49bb-acea-0194c62f73b8";
			let answerId = "f3ffbfe4-8fe2-4bbb-8162-51c1593be284";
			assert.equal(questions.getAnswerWeight(questionId, answerId), 1);
			answerId = "d9624d45-120c-468d-a72c-e46cd4818561";
			assert.equal(questions.getAnswerWeight(questionId, answerId), -2);
		});
		it("should raise error when answerId not found", function() {
			// correct questionId
			let questionId = "89bde201-735e-49bb-acea-0194c62f73b8";
			// wrong answerId
			let answerId = "f3ffbfe4-8fe2-4bbb-8162-51c1593be999";
			assert.throws(
				() => questions.getAnswerWeight(questionId, answerId),
				Error,
			);
			// wrong questionId
			questionId = "89bde201-735e-49bb-acea-0194c62f9999";
			// correct answerId
			answerId = "f3ffbfe4-8fe2-4bbb-8162-51c1593be284";

			assert.throws(
				() => questions.getAnswerWeight(questionId, answerId),
				Error,
			);
		});
	});
});
