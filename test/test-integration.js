const expect = require("chai").expect;
const request = require("request");

it("Loading questions succeeds", function(done) {
	request("http://localhost:8080/api/question?count=1", function(error, response, body) {
		expect(response.statusCode).to.equal(200);
		done();
	});
});

it("Questions response error when unvalid parameter value ", function(done) {
	request("http://localhost:8080/api/question?count=WRONG", function(error, response, body) {
		expect(response.statusCode).to.equal(400);
		done();
	});
});

it("Questions response error when unvalid parameter name ", function(done) {
	request("http://localhost:8080/api/question?questions=3", function(error, response, body) {
		expect(response.statusCode).to.equal(400);
		done();
	});
});

it("Should have valid analyses to valid answers", function(done) {
	const answers = [
		{"questionId": "c0d3dc35-2194-4aae-aebd-ed68cbc9b016", "answerId": "cd9cc7e2-c9f5-46ea-b5af-49534c547f75"},
		{"questionId": "4ba00416-a9bf-4174-bc08-7569b86027c1", "answerId": "665fe05a-efcf-49ef-9fb2-cd7ec63795d1"},
		{"questionId": "b2cb9347-2af6-4911-970b-ee0da3a6ee5f", "answerId": "52e643c3-df64-45f5-a06e-8e6491fd1e64"},
		{"questionId": "fc4ef33a-c0f7-4ebd-a6f8-e49b6a408668", "answerId": "1905f5b2-785a-4a70-bd96-2ebdb0213a5f"},
	];
	request.post({
		url: "http://localhost:8080/api/answer",
		body: answers,
		json: true,
	}, function(error, response, body) {
		expect(response.statusCode).to.equal(200);
		expect(body.evaluation).to.equal("Slightly extrovert");
		done();
	});
});
it("Answers response error when unvalid answers", function(done) {
	const answers = [1, 2, 3];
	request.post({
		url: "http://localhost:8080/api/answer",
		body: answers,
		json: true,
	}, function(error, response, body) {
		expect(response.statusCode).to.equal(400);
		done();
	});
});
