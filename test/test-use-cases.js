const assert = require("assert");
const publishRandomQuestions = require("../src/usecases/publish-random-questions");
const evaluateAnswers = require("../src/usecases/evaluate_aswers");
const inMemoryDataAdapter = require("../src/data-controllers/in-memory/data-adapter-in-memory");
const ResultOutput = require("../src/usecases/models/result-output");
const ErrorOutput = require("../src/usecases/models/error-output");
const Question = require("../src/entities/question");

describe("Use Cases", function() {
	describe("#publishRandomQuestions()", function() {
		it("should test that result is Result class", function() {
			const received = publishRandomQuestions({numberOfQuestions: 4}, inMemoryDataAdapter);
			assert.equal(received instanceof ResultOutput, true);
		});
		it("should test that result is error", function() {
			const received = publishRandomQuestions({numberOfQuestions: 1000}, inMemoryDataAdapter);
			assert.equal(received instanceof ErrorOutput, true);
			assert.deepEqual(new ErrorOutput({
				errorType: ErrorOutput.ERRORTYPE_VALIDATION,
				errorMessage: "Can not get this number of quesitons",
			}), received);
		});
		it("result contains array of (correct amount) Questions", function() {
			const requestedQuestions = 4;
			const received = publishRandomQuestions(
				{numberOfQuestions: requestedQuestions},
				inMemoryDataAdapter,
			);
			assert.equal(received.result.length, requestedQuestions);
			assert.equal(
				received.result.find(
					(elem) => !(elem instanceof Question),
				),
				undefined,
			);
		});
	});
	describe("#evaluateAnswers()", function() {
		it("adds weights and analyses the result (test introverts)", function() {
			const answers = [
				{questionId: "c0d3dc35-2194-4aae-aebd-ed68cbc9b016", answerId: "18d41e53-6d07-4456-a2f3-f3300eb525d4"}, // -1
				{questionId: "4ba00416-a9bf-4174-bc08-7569b86027c1", answerId: "665fe05a-efcf-49ef-9fb2-cd7ec63795d1"}, // -2
				{questionId: "b2cb9347-2af6-4911-970b-ee0da3a6ee5f", answerId: "52e643c3-df64-45f5-a06e-8e6491fd1e64"}, // 1
				{questionId: "fc4ef33a-c0f7-4ebd-a6f8-e49b6a408668", answerId: "0eedd020-89d2-4125-881d-b20ceb9cd13b"}, // -1
			];
			let received = evaluateAnswers({answers}, inMemoryDataAdapter);
			assert.equal(received instanceof ResultOutput, true);
			// Total score of -3 ( < 0 ) => "Slightly intorvert"
			assert.deepEqual({
				result: {evaluation: "Slightly introvert"},
			}, received);

			answers[2] = {questionId: "b2cb9347-2af6-4911-970b-ee0da3a6ee5f", answerId: "61c207ec-41ce-4a22-8e73-ff60fb676e48"}, // -2
			// Total score of -6 ( < -4 ) => "Definitley intorvert"
			received = evaluateAnswers({answers}, inMemoryDataAdapter);
			assert.deepEqual({
				result: {evaluation: "Definitely introvert"},
			}, received);
		});
		it("adds weights and analyses the result (test neutral result)", function() {
			answers = [
				{questionId: "c0d3dc35-2194-4aae-aebd-ed68cbc9b016", answerId: "18d41e53-6d07-4456-a2f3-f3300eb525d4"}, // -1
				{questionId: "4ba00416-a9bf-4174-bc08-7569b86027c1", answerId: "665fe05a-efcf-49ef-9fb2-cd7ec63795d1"}, // -2
				{questionId: "b2cb9347-2af6-4911-970b-ee0da3a6ee5f", answerId: "52e643c3-df64-45f5-a06e-8e6491fd1e64"}, // 1
				{questionId: "fc4ef33a-c0f7-4ebd-a6f8-e49b6a408668", answerId: "1905f5b2-785a-4a70-bd96-2ebdb0213a5f"}, // 2
			];
			// Total score of 0 ( 0 ) => "Could not decide"
			received = evaluateAnswers({answers}, inMemoryDataAdapter);
			assert.deepEqual({
				result: {evaluation: "We could not decide who you are"},
			}, received);
		});
		it("adds weights and analyses the result (test extroverts)", function() {
			const answers = [
				{questionId: "c0d3dc35-2194-4aae-aebd-ed68cbc9b016", answerId: "cd9cc7e2-c9f5-46ea-b5af-49534c547f75"}, // 1
				{questionId: "4ba00416-a9bf-4174-bc08-7569b86027c1", answerId: "665fe05a-efcf-49ef-9fb2-cd7ec63795d1"}, // -2
				{questionId: "b2cb9347-2af6-4911-970b-ee0da3a6ee5f", answerId: "52e643c3-df64-45f5-a06e-8e6491fd1e64"}, // 1
				{questionId: "fc4ef33a-c0f7-4ebd-a6f8-e49b6a408668", answerId: "1905f5b2-785a-4a70-bd96-2ebdb0213a5f"}, // 2
			];
			let received = evaluateAnswers({answers}, inMemoryDataAdapter);
			assert.equal(received instanceof ResultOutput, true);
			// Total score of 2 ( > 0 ) => "Slightly extrovert"
			assert.deepEqual({
				result: {evaluation: "Slightly extrovert"},
			}, received);

			answers[1] = {questionId: "4ba00416-a9bf-4174-bc08-7569b86027c1", answerId: "3f8cba02-59a3-4452-b2c5-f3ae9d099c29"}, // 2
			// Total score of 6 ( > 4 ) => "Definitley extrovert"
			received = evaluateAnswers({answers}, inMemoryDataAdapter);
			assert.deepEqual({
				result: {evaluation: "Definitely extrovert"},
			}, received);
		});
		it("should return ErrorOutput when error in submitted data", function() {
			const answers = [
				{questionId: "c0d3dc35-2194-4aae-aebd-ed68cbc9b016", answerId: "cd9cc7e2-c9f5-46ea-b5af-49534c547f75"},
				{questionId: "4ba00416-a9bf-4174-bc08-7569b86027c1", answerId: "665fe05a-efcf-49ef-9fb2-cd7ec63795d1"},
				{questionId: "b2cb9347-2af6-4911-970b-ee0da3a6ee5f", answerId: "52e643c3-df64-45f5-a06e-8e6491fd1e64"},
				{questionId: "fc4ef33a-c0f7-4ebd-a6f8-e49b6a408668", answerId: "wrong-id"},
			];
			const received = evaluateAnswers({answers}, inMemoryDataAdapter);
			assert.equal(received instanceof ErrorOutput, true);
			assert.deepEqual({
				"errorMessage": "\n\t\t\tCould not find answer wrong-id for question fc4ef33a-c0f7-4ebd-a6f8-e49b6a408668\n\t\t",
				"errorType": "ERROR_UNEXPECTED",
			}, received);
		});
	});
});
