const assert = require("assert");

const AnswerVariant = require("../src/entities/answer-variant");
const Question = require("../src/entities/question");

describe("Entities", function() {
	describe("#class AnswerVariant", function() {
		it("should parse as json string with all relevant data", function() {
			const variant = new AnswerVariant({
				id: "61c207ec-41ce-4a22-8e73-ff60fb676e48",
				description: "Think it’s for the best — it was a lame joke anyway",
				weight: -1,
			});
			assert.equal(
				JSON.stringify(variant),
				"{\"id\":\"61c207ec-41ce-4a22-8e73-ff60fb676e48\",\"description\":\"Think it’s for the best — it was a lame joke anyway\",\"weight\":-1}",
			);
		});
	});
	describe("#class Question", function() {
		it("should parse as json string with all relevant data", function() {
			const question = new Question({
				id: "61c207ec-41ce-4a22-8e73-ff60fb676e48",
				question: "You crack a joke at work, but nobody seems to have noticed. You:",
			});
			question.addAnswer(new AnswerVariant({
				id: "61c207ec-41ce-4a22-8e73-ff60fb676e48",
				description: "Think it’s for the best — it was a lame joke anyway",
				weight: -2,
			}));
			question.addAnswer(new AnswerVariant({
				id: "cfc391e4-0971-4c87-986c-8ac1050d4b2d",
				description: "Keep telling it until they pay attention",
				weight: 2,
			}));
			assert.equal(
				JSON.stringify(question),
				"{\"id\":\"61c207ec-41ce-4a22-8e73-ff60fb676e48\",\"question\":\"You crack a joke at work, but nobody seems to have noticed. You:\",\"variants\":[{\"id\":\"61c207ec-41ce-4a22-8e73-ff60fb676e48\",\"description\":\"Think it’s for the best — it was a lame joke anyway\",\"weight\":-2},{\"id\":\"cfc391e4-0971-4c87-986c-8ac1050d4b2d\",\"description\":\"Keep telling it until they pay attention\",\"weight\":2}]}",
			);
		});
	});
});
